import telebot
from telebot import types
from handler.ChatAi import ChatAi

if __name__ == "__main__":
    chatBot = ChatAi('ai-forever/ruGPT-3.5-13B')
    chatBot.createModel()

    bot = telebot.TeleBot('API_TOKEN')

    @bot.message_handler(commands=['start'])
    def start(message):
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        bot.send_message(message.from_user.id, "ChatBot", reply_markup=markup)


    @bot.message_handler(content_types=['text'])
    def getTextMessages(message):
        bot.send_message(message.chat.id, chatBot.getAnswer(message.text))


    bot.polling(none_stop=True, interval=0)
