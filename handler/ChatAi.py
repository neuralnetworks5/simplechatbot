import torch
from telebot import types
from transformers import AutoModelForCausalLM, AutoTokenizer, pipeline, BitsAndBytesConfig
import bitsandbytes
import accelerate


class ChatAi:
    def __init__(self, name: str) -> None:
        self.name = name
        self.config = BitsAndBytesConfig(load_in_4bit=True)
        self.model = None
        self.pipe = None

    def createModel(self) -> None:
        model = AutoModelForCausalLM.from_pretrained(
            self.name,
            torch_dtype=torch.float16,
            device_map="auto",
            quantization_config=self.config,
        )
        tokenizer = AutoTokenizer.from_pretrained(self.name)
        self.pipe = pipeline('text-generation', model=model, tokenizer=tokenizer)

    def getAnswer(self, message: str) -> str:
        output = self.pipe(
            message,
            max_new_tokens=256,
            top_k=40,
            top_p=0.85,
            repetition_penalty=1.1,
            do_sample=True,
            use_cache=False,
        )
        return output[0]['generated_text']
